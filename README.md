# TECHNICAL_TESTS

__Tests techniques codés en PYTHON 3 et en JAVA 17 :__

![Python3 Codes - Texte alternatif][monimagepython]

[monimagepython]: /PYTHON_3.10.2/images/data_science_machine_learning_python_data_analysis.png "Python3 Codes"

![Java17 Codes - Texte alternatif][monimagejava]

[monimagejava]: /JAVA_17.0.2/src/main/resources/images/data_science_machine_learning_java_data_analysis.png "Java17 Codes"

## Test n°1 (à faire en Python ou en Java)

Ecrire un programme permettant le plus simplement possible de demander à l’utilisateur un nombre et d’afficher tous les nombres pairs compris entre 0 et celui-ci.

__Voir le fichier Python dans le repertoire suivant : [/PYTHON_3.10.2/even_numbers.py](/PYTHON_3.10.2/even_numbers.py)__

__Voir le fichier Java dans le repertoire suivant : [/JAVA_17.0.2/src/main/java/EvenNumbers.java](/JAVA_17.0.2/src/main/java/EvenNumbers.java)__

## Test n°2 (à faire en Python ou Java, sans import en considérant que pi = 3,14)

Écrire un programme permettant de calculer le volume d’un cône droit à l’aide d’une hauteur et d’un rayon renseignés par l’utilisateur.

__Voir le fichier Python dans le repertoire suivant : [/PYTHON_3.10.2/cone_volume.py](/PYTHON_3.10.2/cone_volume.py)__

__Voir le fichier Java dans le repertoire suivant : [/JAVA_17.0.2/src/main/java/ConeVolume.java](/JAVA_17.0.2/src/main/java/ConeVolume.java)__
## Test n°3

Créer une page HTML avec un titre suivi d’un paragraphe les 2 étant centrés.

__Voir les fichiers HTML, CSS et JS dans le repertoire suivant : [/HTML5/document](/HTML5/document)__

## Test n°4

Soit une machine fonctionnant sous Windows. L'adressage est de type
DHCP (en IPv4).

Cette machine n'a pas accès au réseau (d'après l'utilisateur).

ipconfig /all indique l'adresse IP 169.254.21.35/16

Quelles sont les causes possibles du problème ?
Comment réagissez-vous à ces différents cas ?

__Voir le fichier Markdown dans le repertoire suivant : [/DHCP_IPv4/README.md](/DHCP_IPv4/README.md)__

## Test bonus (à faire en Python ou en Java)

Écrire un programme permettant de calculer votre age en fonction de votre date de naissance.

__Voir le fichier Python dans le repertoire suivant : [/PYTHON_3.10.2/date_of_birth.py](/PYTHON_3.10.2/date_of_birth.py)__

__Voir le fichier Java dans le repertoire suivant : [/JAVA_17.0.2/src/main/java/DateOfBirth.java](/JAVA_17.0.2/src/main/java/DateOfBirth.java)__
