// Programme ConeVolume Java pour trouver la surface et le volume d’un cône
import java.io.*;

// Début de la classe publique ConeVolume
public class ConeVolume {
    // Début de la méthode publique statique vide main(String[] args)
    public static void main(String[] args) throws IOException {
        try {
            BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
            // Entrer la hauteur de votre cône en mètre (nombre positif entier ou décimal)
            System.out.println("Saisir une valeur entière ou décimale qui représente la " +
                    "hauteur de votre cône en mètre : Appuyer sur votre clavier numérique");
            double hauteur= Double.parseDouble(in.readLine());
            System.out.println("la hauteur de votre cône en mètre : "+hauteur);
            // Entrer le rayon de votre cône en mètre (nombre entier ou décimal)
            System.out.println("Saisir une valeur entière ou décimale qui représente le " +
                    "rayon de votre cône en mètre : Appuyer sur votre clavier numérique");
            double rayon= Double.parseDouble(in.readLine());
            System.out.println("le rayon de votre cône en mètre : "+rayon);
            // Calculer la hauteur oblique du cône (dans la variable Surface)
            double Surface = Math.sqrt(rayon * rayon + hauteur * hauteur);
            // Calculer la surface du cône (dans la variable SurfaceArea)
            double SurfaceArea = (Math.PI * rayon * rayon) + (Math.PI * rayon * Surface);
            // Calculer le volume du cône (dans la variable Volume)
            double Volume = (Math.PI * rayon * rayon * hauteur) / 3;
            // Le résultat attendu (surface en mètre carré)
            System.out.println("La surface du cône est : " + SurfaceArea +"m²");
            // Le résultat attendu (volume en mètre cube)
            System.out.println("La volume du cône est : " + Volume +"m³");
            // Message d'accomplissement et de réussite du programme ConeVolume
            System.out.println("La surface en mètre carré et le volume en mètre cube du cône vous ont été transmis.");
        }
        /* Ici, Nous pourrions paramétrer les messages d'accompagnement et d'échec du programme ConeVolume
        en fonction de la problématique rencontrer par l'utilisateur ou le programme ConeVolume */
        catch(Exception echec) {
            // Messages d'accompagnement et d'échec du programme ConeVolume
            System.out.println("Problème d'exécution du programme ConeVolume !!! Merci de bien vouloir vérifier " +
                    "le format des deux valeurs hauteur et rayon du cône que vous avez inséré dans le programme(...réessayer et valider vos saisies !)");
            System.out.println("Le programme ConeVolume à échoué, veuillez relancer le programme " +
                    "pour une nouvelle tentative et tenir informer le gestionnaire du programme, dans le cas échéant !!!!!");
        } finally {
            // Message de fermeture du programme ConeVolume
            System.out.println("Veuillez fermer et quitter le programme ConeVolume !");
        }
    }// Fin de la méthode publique statique vide main(String[] args)
}// Fin de la classe publique ConeVolume
