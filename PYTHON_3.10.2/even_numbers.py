#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""Nombres pairs compris entre zéro et un nombre entier (n) entré par l'utilisateur."""
# Fichier : even_numbers.py
# Auteur : Moussa Belaifa

# Ce script gère la demande à l’utilisateur d'un nombre et d’afficher
# les nombres pairs compris entre 0 et celui-ci.

# Programme principal -----------------------------------------------

# Lire la valeur entière de n
n = input("Entrer la valeur d'un nombre (n) entier : ")

# Convertir n en nombre entier
n = int(float(n))

# Tester si n est pair ou non
if(n%2 == 0):
    print("Le nombre '", n, "' tapé est pair ")
else:
    print("Le nombre '", n, "' tapé est impair ")

# Imprimer la liste des nombres pairs compris entre 0 et votre nombre
evens = [n for n in range(n) if n%2 == 0]
print("Liste des nombres pairs compris entre 0 et votre nombre entier (n) :", evens)

